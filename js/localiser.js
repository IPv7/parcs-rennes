jQuery(document).ready(function () {
    // au clic sur le bouton géolocalisation
    jQuery("#geolocalisation").click(function localiser() {
        // on affiche le spinner
        jQuery("#geo_icon").addClass("hidden")
        jQuery("#geo_spinner").removeClass("hidden")

        // on vire tous les marqueurs du calque Leaflet créé pour contenir le marqueur de géoloc
        marqueurGeoloc.clearLayers();

        // si l'API HTML5 de géolocalisation est disponible
        if (navigator.geolocation) {
            // on récupère la postion
            navigator.geolocation.getCurrentPosition(geo_succes, geo_erreur, geo_options);
        } else {
            alert("Géolocalisation non supportée")
            console.warn("Géolocalisation non supportée")
        }
    })

    // on définit les différents paramètres
    var geo_options = {
        enableHighAccuracy: true,
        timeout: 10000, // si pas de réponse sous 5 secondes : échec
        maximumAge: 120000 // garder la réponse en cache pendant 2mn
    };


    function geo_erreur(erreur) {
        // on masque le spinner
        jQuery("#geo_icon").removeClass("hidden")
        jQuery("#geo_spinner").addClass("hidden")
        // et on remonte l'erreur
        console.error("Erreur de géolocalisation\nest-elle autorisée dans votre navigateur ?\n\n" + JSON.stringify(erreur, null, 2));
        alert("Erreur de géolocalisation\nest-elle autorisée dans votre navigateur ?\n\n" + JSON.stringify(erreur, null, 2));
    }


    function geo_succes(position) {
        // on fait l'appel à l'API pour obtenir le nom de la ville correspondant aux coordonnées GPS
        jQuery.ajax({
            url: "https://nominatim.openstreetmap.org/reverse",
            type: "GET",
            data: "lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&format=json&addressdetails=0&zoom=10",
            success: function (data) { //si la requête est un succès
                // on split la chaine au premier élement pour ne récupère que la ville
                ville = data.display_name.split(",")[0]
                // si la ville n'est pas Rennes
                if(ville != "Rennes"){
                    alert("Géolocalisation non pertinente : vous n'êtes pas à Rennes ! 😉")
                } else {
                    // on initialise le marqueur pour la géolocalisation
                    var icone_rouge = L.icon({
                        iconUrl: 'https://i.ya-webdesign.com/images/maps-pin-png.png',
                        iconSize: [25, 41],
                        iconAnchor: [0, 20]
                    });
            
                    // et on l'ajoute aux coordonnées localisées
                    L.marker([position.coords.latitude, position.coords.longitude], {icon: icone_rouge}).addTo(marqueurGeoloc).bindPopup("Je suis ici 🙃").openPopup();
                }
               
            },
            error: function (data) {
                // on remonte l'erreur
                alert("Erreur de requête API pour obtenir la ville à partir des coordonées GPS\n\n" + JSON.stringify(data, null, 2))
                console.error("Erreur de requête API pour obtenir la ville à partir des coordonées GPS\n\n" + JSON.stringify(data, null, 2))
            },
            complete: function () {
                // quand la fonction est terminée on enlève le spinner
                jQuery("#geo_icon").removeClass("hidden")
                jQuery("#geo_spinner").addClass("hidden")
            }

        })

        
    }



})