jQuery("#appliquer-filtres").click(function appliquerFiltres() {
    // on affiche un spinner le temps du traitement pour signifier à l'utilisateur qu'il faut attendre
    jQuery("#appliquer_spinner").removeClass("hidden")
    jQuery("#appliquer_logo").addClass("hidden")
    // clear de la carte
    marqueursParcs.clearLayers();
    var filtre;
    filtre = app._data.filtresChecked; // récupération des choix faits par l'utilisateur
    for (i in app._data.retour_api.data.records) {
        var discrimination_eau = true; // variable pour savoir si l'on doit afficher le parc

    /////// test eau ///////

        //test si aucun coché
        if (! filtre.includes("eau-oui") &&! filtre.includes("eau-non") && (typeof app._data.retour_api.data.records[i].fields.borne_eau === 'undefined'  || app._data.retour_api.data.records[i].fields.borne_eau === null)) {
            discrimination_eau = false;
        }
        //test eau si oui et non cochés
        if(discrimination_eau){
            if (filtre.includes("eau-oui") && filtre.includes("eau-non")) {
                discrimination_eau = false;
            }
        }           
        //test eau si oui coché
        if (discrimination_eau){
            if (filtre.includes("eau-oui") &&! filtre.includes("eau-non") && app._data.retour_api.data.records[i].fields.borne_eau == "oui" ) {
                discrimination_eau = false;
            }
        }
        //test eau si non coché
        if (discrimination_eau){
            if (! filtre.includes("eau-oui") && filtre.includes("eau-non") && app._data.retour_api.data.records[i].fields.borne_eau == "non") {
                discrimination_eau = false;
            }
        }

    /////// test éclairage ///////
        var discrimination_ecl = true;// variable pour savoir si l'on doit afficher le parc
        //test si aucun coché
        if (! filtre.includes("ecl-oui") &&! filtre.includes("ecl-non") && (typeof app._data.retour_api.data.records[i].fields.eclairage === 'undefined'  || app._data.retour_api.data.records[i].fields.eclairage === null)) {
            discrimination_ecl = false;
        }
        //test ecl si oui et non cochés
        if(discrimination_ecl){
            if (filtre.includes("ecl-oui") && filtre.includes("ecl-non")) {
                discrimination_ecl = false;
            }
        }           
        //test ecl si oui coché
        if (discrimination_ecl){
            if (filtre.includes("ecl-oui") &&! filtre.includes("ecl-non") && app._data.retour_api.data.records[i].fields.eclairage == "oui" ) {
                discrimination_ecl = false;
            }
        }
        //test ecl si non coché
        if (discrimination_ecl){
            if (! filtre.includes("ecl-oui") && filtre.includes("ecl-non") && app._data.retour_api.data.records[i].fields.eclairage == "non") {
                discrimination_ecl = false;
            }
        }

    /////// test public ///////
        var discrimination_pub = true;// variable pour savoir si l'on doit afficher le parc
         //test si aucun coché
        if (! filtre.includes("pub-oui") &&! filtre.includes("pub-non") && (typeof app._data.retour_api.data.records[i].fields.espace_publique === 'undefined'  || app._data.retour_api.data.records[i].fields.espace_publique === null)) {
            discrimination_pub = false;
        }
        //test pub si oui et non cochés
        if(discrimination_pub){
            if (filtre.includes("pub-oui") && filtre.includes("pub-non")) {
                discrimination_pub = false;
            }
        }           
        //test pub si oui coché
        if (discrimination_pub){
            if (filtre.includes("pub-oui") &&! filtre.includes("pub-non") && app._data.retour_api.data.records[i].fields.espace_publique == "oui" ) {
                discrimination_pub = false;
            }
        }
        //test pub si non coché
        if (discrimination_pub){
            if (! filtre.includes("pub-oui") && filtre.includes("pub-non") && app._data.retour_api.data.records[i].fields.espace_publique == "non") {
                discrimination_pub = false;
            }
        }

    /////// ajout du point si les 3 variables de discriminations sont valides///////
        if (! discrimination_eau &&! discrimination_ecl &&! discrimination_pub){
            ajouterUnParc(app._data.retour_api.data.records[i]);
        }
    }

    jQuery("#appliquer_spinner").addClass("hidden")
    jQuery("#appliquer_logo").removeClass("hidden")
})