// source : https://nouvelle-techno.fr/actualites/pas-a-pas-inserer-une-carte-openstreetmap-sur-votre-site
var lat_rennes = 48.112771;
var lon_rennes = -1.680033;

var macarte = null;


// Nous initialisons le groupe de marqueurs pour les parcs
var marqueursParcs = L.layerGroup();
// Nous initialisons le groupe de marqueurs pour le marqueur de géoloc
var marqueurGeoloc = L.layerGroup();


// Fonction d'initialisation de la carte
function initialiser_map() {

    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map_leaflet"
    // avec 2 layers ("calques") qui servent à différencier les marqueurs pour les parcs et le marqueurs pour la géolocalisation
    macarte = L.map('map', {
        center: [lat_rennes, lon_rennes],
        zoom: 13,
        fullscreenControl: true,
        layers: [marqueursParcs, marqueurGeoloc]
    });



    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 12,
        maxZoom: 18
    }).addTo(macarte);



}

// pour ajouter tous les parcs du jeu de données original de l'API
function ajouterTousLesParcs(records_api) {
    for (parc in records_api) {
        ajouterUnParc(records_api[parc])
    }
}

// fonction pour ajouter un parc depuis un "parc" du jeu de données
function ajouterUnParc(valeurRecord) {

    
    affichageEspacePublic = ""
    // si le champ est défini dans la réponse de l'API, alors on l'ajoute à la carte
    if (typeof valeurRecord.fields.espace_publique == 'string') {
        // si la longueur de la valeure est supérieure à 2
        if (valeurRecord.fields.nom.length > 2) {
            affichageEspacePublic = "<br>Public : " + valeurRecord.fields.espace_publique
        }
    }

    affichageEclairage = ""
    if (typeof valeurRecord.fields.eclairage == 'string') {
        if (valeurRecord.fields.nom.length > 2) {
            affichageEclairage = "<br>Eclairage : " + valeurRecord.fields.eclairage
        }
    }

    affichageBorneEau = ""
    if (typeof valeurRecord.fields.borne_eau == 'string') {
        if (valeurRecord.fields.nom.length > 2) {
            affichageBorneEau = "<br>Borne eau : " + valeurRecord.fields.borne_eau
        }
    }


    nomParc = "<span class='popupMap'>Parc inconnu</span>"
    if (typeof valeurRecord.fields.nom == 'string') {
        if (valeurRecord.fields.nom.length > 2) {
            nomParc = "<span class='popupMap'>" + valeurRecord.fields.nom + "</span>"
        }
    }

    // ajout du marqueur au layer marqueursParcs
    L.marker([valeurRecord.fields.geo_point_2d[0], valeurRecord.fields.geo_point_2d[1]]).bindPopup(nomParc + affichageEspacePublic + affichageBorneEau + affichageEclairage).addTo(marqueursParcs);

}