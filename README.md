# Parcs Rennes

Site web réalisé en TP avec Bootstrap et le framework VueJS.\
Fait une requête à l'API [de Rennes Métropole](https://data.rennesmetropole.fr/api/records/1.0/search//?dataset=aires-de-jeux-des-espaces-verts-rennes) puis affiche les résultats.\
Accessible à l'adresse [https://parcs-rennes.IPv7.fr](https://parcs-rennes.IPv7.fr)